CFLAGS=-Wall -Wextra -g
NAME=split-audio-file

all:${NAME}

${NAME}: main.o
	$(CC) $^ -o $@
main.o: main.c

clean:
	@rm main.o ${NAME}
