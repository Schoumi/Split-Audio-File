# Split Audio File

Split audio file is a mini-application to split a long vorbis (ogg) file into segment and add metadata for each part.

# Build

```
make
```

# Run

```
# OutputDir should exist before runing

./split-audio-file -i InputFile.ogg -o OutputDir -s SplitInfoFile.csv
```

# SplitIntoFile File Format

```
group;A Group;
album;An Album;
year;2019;
genre;A Genre;
tracks;5;
1;Title 1;00:00;01:50;
2;Title 2;01:50;05:43;
3;Title 3;05:43;09:53;
4;Title 4;09:53;13:49;
5;Title 5;13:49;17:20;
```

group, album, year, genre are optionnal

tracks is mandatory

tracknumber;title;start_time;end_time; are mandatory


# More

It's a simple peace of code made in few hours, it could be inprove in many ways.

<br/>
<noscript><a href="https://liberapay.com/Schoumi/donate"><img src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
